#Author-Dag Rende
#Description-Sketch tool to create a spiral

# Select a centerpoint min diameter, pitch and the number of turns

# Thanks to Casey Rogers, Patrick Rainsberry, David Liu and Gary Singer - the creators of the Dogbone 
# add-in - for a clean and simple Fusion 360 add-in template.

from collections import defaultdict

import adsk.core, adsk.fusion
import math
import traceback
import re
import os
import random

# pip install pyyaml -t packages; mv packages/yaml .; rm -rf packages
from . import yaml

import time
from . import utils

class SpiralCommand(object):
    COMMAND_ID = "spiralBtn"

    def __init__(self):
        self.app = adsk.core.Application.get()
        self.ui = self.app.userInterface

        self.centerPoint = None
        self.minDiameter = 0
        self.pitch = 0
        self.revolutions = 0
        self.revPoints = 0

        self.defaults = {
            'minDiameter': "10 mm",
            'pitch': "5 mm",
            'revolutions': "2",
            'revPoints': "16"
            }

        self.handlers = utils.HandlerHelper()
        self.appPath = os.path.dirname(os.path.abspath(__file__))

    def writeDefaults(self):
        try:
            with open(os.path.join(self.appPath, 'defaults.yml'), 'w') as file:
                documents = yaml.dump(self.defaults, file)
        except:
            pass
    
    def readDefaults(self): 
        if not os.path.isfile(os.path.join(self.appPath, 'defaults.yml')):
            return
        with open(os.path.join(self.appPath, 'defaults.yml'), 'r') as file:
            self.defaults.update(yaml.safe_load(file))


    def addButton(self):
        # clean up any crashed instances of the button if existing
        try:
            self.removeButton()
        except:
            pass

        # add add-in to UI
        button = self.ui.commandDefinitions.addButtonDefinition(
            self.COMMAND_ID, 'Spiral', 'Creates a spiral', 'Resources')

        button.commandCreated.add(self.handlers.make_handler(adsk.core.CommandCreatedEventHandler,
                                                                    self.onCreate))

        createPanel = self.ui.allToolbarPanels.itemById('SketchPanel')
        buttonControl = createPanel.controls.addCommand(button, self.COMMAND_ID)

        # Make the button available in the panel.
        buttonControl.isPromotedByDefault = True
        buttonControl.isPromoted = True

    def removeButton(self):
        cmdDef = self.ui.commandDefinitions.itemById(self.COMMAND_ID)
        if cmdDef:
            cmdDef.deleteMe()
        createPanel = self.ui.allToolbarPanels.itemById('SketchPanel')
        cntrl = createPanel.controls.itemById(self.COMMAND_ID)
        if cntrl:
            cntrl.deleteMe()

    def onCreate(self, args):
        self.readDefaults()

        inputs = args.command.commandInputs
        args.command.setDialogInitialSize(425, 475)
        args.command.setDialogMinimumSize(425, 475)
        args.command.tooltip = "Select center point of spiral."


        centerPoint = inputs.addSelectionInput(
            'centerPoint', 'Center Point',
            'Select center point of spiral')
        centerPoint.addSelectionFilter('SketchPoints')
        centerPoint.setSelectionLimits(1,1)

        minDiameter = inputs.addValueInput(
            'minDiameter', 'Minimum Diameter', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['minDiameter']))
        minDiameter.tooltip = "Inner diameter of spiral."

        pitch = inputs.addValueInput(
            'pitch', 'Pitch', self.design.unitsManager.defaultLengthUnits,
            adsk.core.ValueInput.createByString(self.defaults['pitch']))
        pitch.tooltip = "Diameter difference for one spiral revolution."

        revolutions = inputs.addFloatSpinnerCommandInput(
            'revolutions', 'Revolutions', '',
            0.0, 1000.0, 1.0, 
            1.0)
        revolutions.expression = self.defaults['revolutions']
        revolutions.tooltip = "Number of revolutions of the spiral."

        revPoints = inputs.addFloatSpinnerCommandInput(
            'revPoints', 'Points per revolution', '',
            0.0, 100.0, 1.0, 
            16.0)
        revPoints.expression = self.defaults['revPoints']
        revPoints.tooltip = 'the number of control points per revolution.'

        
        # Add handlers to this command.
        args.command.execute.add(self.handlers.make_handler(adsk.core.CommandEventHandler, self.onExecute))
        args.command.validateInputs.add(
            self.handlers.make_handler(adsk.core.ValidateInputsEventHandler, self.onValidate))
        args.command.inputChanged.add(
            self.handlers.make_handler(adsk.core.InputChangedEventHandler, self.onInputChanged))

    def parseInputs(self, inputItems):
        inputs = {inp.id: inp for inp in inputItems}

        self.centerPoint = None
        self.centerPoint = inputs['centerPoint']
        self.defaults['minDiameter'] = inputs['minDiameter'].expression
        self.minDiameter = inputs['minDiameter'].value
        self.defaults['pitch'] = inputs['pitch'].expression
        self.pitch = inputs['pitch'].value
        self.defaults['revolutions'] = inputs['revolutions'].expression
        self.revolutions = inputs['revolutions'].value
        self.defaults['revPoints'] = inputs['revPoints'].expression
        self.revPoints = inputs['revPoints'].value

    def onValidate(self, args):
        self.log('onValidate')
        cmd = args.firingEvent.sender
        for input in cmd.commandInputs:
            if input.id == 'centerPoint':
                self.log('centerPoint selectionCount={}'.format(input.selectionCount))
                if input.selectionCount < 1:
                    args.areInputsValid = False

    def onInputChanged(self, args):
        self.log('onInputChanged')
        cmd = args.firingEvent.sender

    def onExecute(self, args):
        app = adsk.core.Application.get()
        start = time.time()
        doc = app.activeDocument  
        design = app.activeProduct
        timeLine = design.timeline
        timeLineGroups = timeLine.timelineGroups
        timelineCurrentIndex = timeLine.markerPosition
        
        self.parseInputs(args.firingEvent.sender.commandInputs)
        self.writeDefaults()
        self.createSpiral()
        
        timelineEndIndex = timeLine.markerPosition
        # if timelineEndIndex - timelineCurrentIndex > 1:
        #     exportTimelineGroup = timeLineGroups.add(timelineCurrentIndex, timelineEndIndex-1)# the minus 1 thing works, weird.
        #     exportTimelineGroup.name = 'Spiral'
        


    @property
    def design(self):
        return self.app.activeProduct

    @property
    def rootComp(self):
        return self.design.rootComponent

    def log(self, msg):
        try:
            with open(os.path.join(self.appPath, 'log.txt'), 'a+') as file:
                file.write(msg + "\n")
        except:
            pass

    def createSpiral(self):
        if not self.design:
            raise RuntimeError('No active Fusion design')

        app = adsk.core.Application.get()
        design = app.activeProduct
        sketch = app.activeEditObject
        if not type(sketch) is adsk.fusion.Sketch:
            ui.messageBox("Only works in an open sketch")
            return None

        vStep = 2.0 * math.pi / self.revPoints
        totV = self.revolutions * 2 * math.pi
        outerRadius = self.minDiameter / 2 + self.pitch * self.revolutions

        points = adsk.core.ObjectCollection.create()

        def addPoint(r, v):
            p1 = adsk.core.Point3D.create(r * math.cos(v), r * math.sin(v))
            points.add(p1)

        v = 0
        b = self.pitch / (2 * math.pi)
        while v < totV:
            r = self.minDiameter / 2 + b * v
            addPoint(r, v)
            v += vStep
        # add last point
        addPoint(outerRadius, totV)

        sketch.sketchCurves.sketchFittedSplines.add(points) 
 


spiralCommand = SpiralCommand()


def run(context):
    try:
        spiralCommand.addButton()
    except:
        utils.messageBox(traceback.format_exc())


def stop(context):
    try:
        spiralCommand.removeButton()
    except:
        utils.messageBox(traceback.format_exc())
