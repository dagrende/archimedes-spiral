# Create Archimetes spiral path

This add-in adds a spiral tool to the Sketch tool-bar.

## Installation

1. Download this repository as a zip file
1. Unpack it into the Fusion 360 Add-in directory
1. Restart Fusion 360
1. Open a Sketch and the new spiral button should be in the Sketch toolbar.

## Usage

1. Create or open a sketch
1. Click the Spiral button
1. Select a centerpoint
1. Specify inner diameter, pitch (distance between revolutions) and number of revolutions
1. Click OK

A spline curve approximating a spiral is created around the centerpoint.

To give the spiral a body, it is easiest to create a new sketch perpendicuar to the spiral path with a shape, and use the Sweep tool selecting the shape and the spiral path.
